/**
 * 
 */

angular.module("app").factory("categoriaAPI", function($http, config){
	var _listarTodos = function(){
		return $http.get(config.baseUrl + "/services/api/categoria/listar"
				);
	}
	
	var _buscarPorId = function(id){
		return $http.get(config.baseUrl + "/services/api/categoria/buscarPorId/"+id);
	}
	
	return {
		listarTodos: _listarTodos,
		buscarPorId: _buscarPorId
	}
})