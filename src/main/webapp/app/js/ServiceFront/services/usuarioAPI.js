angular.module("app").factory(
		"usuarioAPI",
		function($http, config) {

			var _cadastrarUsuario = function(usuario) {
				return $http.post(config.baseUrl
						+ "/services/api/usuario/cadastrar", usuario);
			}

			var _excluirUsuario = function(idUsuario) {
				return $http.post(config.baseUrl + "/services/api/urlNetBeans",
						idUsuario);
			}

			var _editarUsuario = function(usuario) {
				return $http.post(config.baseUrl 
						+ "/services/api/usuario/editar", usuario);
			}

			var _pegarUsuario = function(id) {
				return $http.get(config.baseUrl + "/services/api/usuario/buscarPorId/"+id);
			}

			var _ativarUsuario = function(idUsuario) {
				return $http.post(config.baseUrl + "/services/api/urlNetBeans",
						idUsuario);
			}

			var _logar = function(usuario) {
				return $http.post(config.baseUrl + "/services/api/usuario/autenticar", usuario);
			}
			
			var _listarTodos = function(){
				return $http.get(config.baseUrl + "/services/api/usuario/listar"
						);
			}

			function error(error) {
				return function() {
					return {
						success : false,
						message : error
					};
				};
			}
			function success(res) {
				return res.data;
			}

			return {
				cadastrarUsuario : _cadastrarUsuario,
				excluirUsuario : _excluirUsuario,
				editarUsuario : _editarUsuario,
				pegarUsuario : _pegarUsuario,
				listarTodos: _listarTodos,
				ativarUsuario : _ativarUsuario,
				logar : _logar
			}
		});