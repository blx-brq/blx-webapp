/**
 * tenho ciumes desse codigo mesmo u-u não faz merda nele 
 */

var app = angular.module('app', [ 'ui.router', 'ui.materialize', 'angular.viacep', 'ngResource', 'angularUtils.directives.dirPagination', 'ngCookies' ]);

app.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

	$urlRouterProvider.otherwise('login');

	$stateProvider.state('login', {
		url : "/login",
		templateUrl : "partials/login.html",
		controller : "loginCtrl",
		data : {
			requireLogin : false
		}/*,
		onEnter : function ($state, $modal) {
			$modal.open({
				templateUrl: "partials/modal/modalLogin.html",
				controller: ""
			}).result.then(function () {
				//$state.go('^',{},{reload:true});
				alert("primeira function");
			}),function () {
				alert("segunda function");
			}
		}
		*/
	})

	.state('userCadastro', {
		url : "/userCadastro",
		templateUrl : "partials/userCadastro.html",
		controller : "userCadastroCtrl",
		reload: true,
		data : {
			requireLogin : false
		}
	})

	.state('accountAdmin', {
		url : "/accountAdmin",
		templateUrl : "partials/homeAdmin.html",
		data : {
			requireLogin : true
		}
	})

	.state('accountUser', {
		url : "/accountUser",
		templateUrl : "partials/homeUser.html",
		controller: "homeCtrl",
		data : {
			requireLogin : true
		}
	})

	.state('novoAnuncio', {
		url : "/novoAnuncio",
		templateUrl : "partials/novoAnuncio.html",
		controller : "novoAnuncioCtrl",
		data : {
			requireLogin : true
		}
	})

	.state('alterarAnuncio', {
		url : "/alterarAnuncio",
		templateUrl : "partials/alterarAnuncio.html",
		data : {
			requireLogin : true
		}
	})

	.state('listarAnuncios', {
		url : "/anuncios",
		templateUrl : "partials/listarAnuncios.html",
		controller : "anuncioCtrl",
		data : {
			requireLogin : true
		}
	})

	.state('listarAnunciosUsuario', {
		url : "/meus_anuncios",
		templateUrl : "partials/listarAnunciosUsuario.html",
		controller : "listarAnuncioUserCtrl",
		data : {
			requireLogin : true
		}
	})

	.state('listarAnunciosAdmin', {
		url : "/anuncios_admin",
		templateUrl : "partials/listarAnunciosAdmin.html",
		controller : "listarAnuncioAdmCtrl",
		data : {
			requireLogin : true
		}
	})

	.state('usuarios', {
		url : "/usuarios",
		templateUrl : "partials/usuarios.html",
		controller : "usuariosCtrl",
		data : {
			requireLogin : true
		}
	})

	.state('perfil', {
		url : "/perfil",
		templateUrl : "partials/perfilUsuario.html",
		controller : "perfilCtrl",
		data : {
			requireLogin : true
		}
	})
	
	.state('alterarUsuario', {
		url : "/alterarPerfil",
		templateUrl : "partials/alterarUsuario.html",
		controller: "alterarUsuarioCtrl",
		data : {
			requireLogin : true
		}
	})

	.state('alterarUsuarioAdmin', {
		url: "/alterarPerfilAdmin/:codUsuario",	
		params: {
			codUsuario : null
		},		
		templateUrl : "partials/alterarUsuarioAdm.html",
		controller: "editarUserCtrl",
		data: {
			requireLogin: true
		}
	
	})

	.state('erro', {
		url : "/erro",
		templateUrl : "partials/erro.html",
		data : {
			requireLogin : false
		}

	})
	
	.state('logout', {
	    url: '/logout',
	    controller: "logoutCtrl",
	    redirectTo: "login",
	    reload: true,
	    data : {
	    	requireLogin: false
	    }
	}) 

});

app.run(function($rootScope, $location, $state, $cookies, $timeout) {
	$rootScope.$on('$stateChangeStart', function(event, toState, toParams,
			fromState, fromParams, options) {

		console.log("cookies");
		console.log($cookies);
		
	})
	// keep user logged in after page refresh
	
	$rootScope.globals = $cookies.get('globals') || {};
	if ($rootScope.globals.currentUser) {
		 //$http.defaults.headers.common['Authorization'] = 'Basic ' +  //tipo de autorização.. as vezes está comentada para testes do via cep em novo anuncio 
		 $rootScope.globals.currentUser.authdata; // jshint ignore:line
		 //$timeout(2000);
	}

	$rootScope.$on('$locationChangeStart', function(event, next, current) {
		// redirect to login page if not logged in
		if ($location.path() !== '/login' && !$rootScope.globals.currentUser
				&& $location.path() !== '/userCadastro') {
			$location.path('/login');
			$location.path('/userCadastro');
		}

	});

});