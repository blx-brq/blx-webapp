/**
 * 
 */

angular.module("app").controller("usuariosCtrl", function($rootScope, $location, $scope, usuarioAPI) {
	
	if( $rootScope.globals.currentUser.id != ' ' ) {
		usuarioAPI.pegarUsuario($rootScope.globals.currentUser.id)
			.success(function(data) {
				$scope.usuario = data;
				console.log($scope.usuario);
				console.log($scope.usuario.blxContato);
			}).error(function(erro) {
				$state.go('erro'); 
			});
	}
	
	usuarioAPI.listarTodos().then(function(response) {
		$scope.usuarios = response.data;
		console.log($scope.usuarios);
	});

	$scope.desativar = function() {
		console.log($scope.usuario);
	};
	$scope.ativar = function() {
		console.log($scope.usuario);
	};

});