/**
 * 
 */

angular.module("app").controller("loginCtrl",function($rootScope, $location, $scope, $cookies, $state, AuthenticationService){
	$rootScope.activetab = $location.path();

    $scope.msg = {};

    /*
    $modal.open({
        templateUrl:'partials/modal/modalLogin.html'

    });
	/*
	$scope.validaLogin = function(usuario){
		
	}
	*/
	AuthenticationService.ClearCredentials();
	  
    $scope.login = function () {
        $scope.dataLoading = true;
        AuthenticationService.Login($scope.usuario, function(response) {
        	console.log(response);
            if(response != null) {
                AuthenticationService.SetCredentials(response.vlLogin, response.vlSenha, response.codUsuario);
                if(response.blxTipoUsuario.codTipoUsuario == 1) {
                	$state.go('accountAdmin');
                } else if(response.blxTipoUsuario.codTipoUsuario == 2){
                	$state.go('accountUser');
                } else{
                    $scope.msg = "Login ou Senha incorretos";
                    console.log(msg);
                }
            } else {
            	$state.go('erro');
                //$scope.dataLoading = false;
            }
        });
    };
	
});
