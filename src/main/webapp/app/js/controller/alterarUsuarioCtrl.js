/**
 * 
 */

angular.module("app").controller("alterarUsuarioCtrl", function($rootScope, $location, $scope, $http, $state, usuarioAPI) {
		
	if( $rootScope.globals.currentUser.id != ' ' ) {
		usuarioAPI.pegarUsuario($rootScope.globals.currentUser.id)
			.success(function(data) {
				console.log(data);
				$scope.usuario = data;
			}).error(function(erro) {
				$state.go('erro'); 
			});
	}
	
	$scope.adicionarCadastro = function(usuario, contato) {
		if ($scope.usuario == null)
				alert("Preencha todos os campos!");	
		else {
			$scope.usuario.blxContato = $scope.contato;
			console.log(usuario);
			usuarioAPI.editarUsuario(usuario)
				.success(function(data) {
					console.log(data);
				}).error(function(erro) {
					$state.go('erro'); 
				});
		}
	}
})