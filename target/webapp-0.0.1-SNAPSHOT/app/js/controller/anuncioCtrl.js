/**
 * antigo detalheAnuncio.. tela do givanni 
 */

angular.module("app").controller("novoAnuncioCtrl", function($rootScope, $location, $scope) {
	
	$scope.categorias = [
			{nome: 'Eletrônicos', subcategorias: [{nome: 'TV'}, {nome: 'PC'}, {nome: 'Notebooks'}] },
			{nome: 'Vestuário', subcategorias: [{nome: 'TV'}, {nome: 'PC'}, {nome: 'Notebooks'}]},
			{nome: 'Perfumaria', subcategorias: [{nome: 'TV'}, {nome: 'PC'}, {nome: 'Notebooks'}]},
			{nome: 'Móveis', subcategorias: [{nome: 'TV'}, {nome: 'PC'}, {nome: 'Notebooks'}]},
			{nome: 'Eletrodomésticos', subcategorias: [{nome: 'TV'}, {nome: 'PC'}, {nome: 'Notebooks'}]}
		];
	
	$scope.notas = [
		     				{nota: 1, descricao: 'ruim'},
		     				{nota: 2, descricao: 'regular'},
		     				{nota: 3, descricao: 'bom'},
		     				{nota: 4, descricao: 'muito bom'},
		     				{nota: 5, descricao: 'excelente'},
		     			];
	
		$scope.anuncio = {
				nome: 'Iphone 5s - Seminovo',
				descricao: 'Aparelho pouco usado, muito bom, sem nenhum defeito. Funciona perfeitamente.',
				preco: '1500',
				uf: 'PR',
				cidade: 'Curitiba',
				data: '29/06/2016',
				telfixo: '(41) 3333-3333',
				telmovel: '(41) 9999-9999',
				nota: 3.5
		};
		$scope.estrelas = [];
		$scope.getEstrelas = function(nota) {
			var nt = nota;		
			for (var i = 0; i < 5; i++) {
				if (nt >= 1.0) {
					$scope.estrelas.push("star");
				}
				else if (nt < 1 && nt >  0) {
					$scope.estrelas.push("star_half");
				}
				else {
					$scope.estrelas.push("star_border");
				}
				nt--;
			}
		};

		$scope.getEstrelas($scope.anuncio.nota);
		
});